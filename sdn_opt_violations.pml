/* Count end-host MUST be at least 2. */
#define COUNT_ENDHOST 3
#define COUNT_TOTAL COUNT_ENDHOST + 2
#define ATTACKER_ID 0
#define SDN_SWITCH_ID COUNT_TOTAL - 2
#define SWITCH_IP SDN_SWITCH_ID + 1
#define SDN_CONTROLLER_ID COUNT_TOTAL - 1
#define DHCP_SERVER_ID COUNT_TOTAL - 3
#define ARP_NO_ENTRY 0
#define NO_ENTRY 0
#define IP_BROADCAST 15
#define ETH_BROADCAST 15
#define IP_NULL      0
#define MESSAGE_QUEUE_CAPACITY 2
#define IP_BITS 3
#define MAC_BITS 3
#define PORT_BITS 3
#define ACTOR_BITS 3
#define ATOMIC atomic

/*
 * In this version of the model all layer headers are unpacked in a single one,
 * thus all the types (also protocol/layer specific) are here.
 */
mtype = { ARP_REQUEST, ARP_REPLY, DHCP_OFFER, DHCP_DISCOVER, DHCP_REQUEST, DHCP_ACK, 
          DHCP_NAK, DHCP_DECLINE, DHCP_RELEASE, ICMP_PING, SDN_PACKET };

/*
 * ARP
 * The following fields are omitted to save space.
 * They are not useful anyway.
 * - arhrd (Hardware address space)
 * - arpro (Protocol address space)
 * - arhln (Hardware address unsigned-length)
 * - arpln (Protocol address unsigned-length)
 *
 * DHCP
 * We're excluding several fields, also usually important ones like lease time.
 * We also skip giaddr, ciaddr, boot fields. Also "xid" is skipped.
 * In our attack model, the attacker cannot control the switches, thus we 
 * can assume that the nonce cannot be retrieved by it (and so we identify
 * checking the packed cid directly)
 * ciaddr is client ip address (useful only BOUND, RENEW, REBINDING
   ... and that's the reason we're omitting it here) 
 */
typedef message {
	mtype type;
	byte meta;
	byte eth_header;
	byte ip_header;
	byte extra;
};

/* Message unpack macros */
#define get_type(m) (m.type)
#define get_originalType(m) (m.meta & 31)
#define get_cid(m) ((m.meta >> 5) & 7)
#define set_type(m,v) m.type = v
#define set_originalType(m,v) m.meta = (v & 224) | v
#define set_cid(m,v) m.meta = (v & 31) | (v << 5)
#define get_eth_src(m) (m.eth_header & 15)
#define get_eth_dst(m) ((m.eth_header >> 4) & 15)
#define set_eth_src(m,v) m.eth_header = ((m.eth_header & 240) | v)
#define set_eth_dst(m,v) m.eth_header = ((m.eth_header & 15) | (v << 4))
#define get_nw_src(m) (m.ip_header & 15)
#define get_nw_dst(m) ((m.ip_header >> 4) & 15)
#define set_nw_src(m,v) m.ip_header = ((m.ip_header & 240) | v)
#define set_nw_dst(m,v) m.ip_header = ((m.ip_header & 15) | (v << 4))
#define get_siaddr(m) (m.extra & 15)
#define get_yiaddr(m) ((m.extra >> 4) & 15)
#define get_chaddr(m) get_eth_src(m)
#define set_siaddr(m,v) m.extra = ((m.extra & 240) | v)
#define set_yiaddr(m,v) m.extra = ((m.extra & 15) | (v << 4))
#define set_chaddr(m,v) set_eth_src(m,v)
#define get_artha(m) (m.extra & 3)
#define get_artpa(m) ((m.extra >> 2) & 7)
#define get_arspa(m) ((m.extra >> 5) & 3)
#define get_arsha(m) get_eth_src(m)
#define set_artha(m,v) m.extra = ((m.extra & 252) |  v)
#define set_artpa(m,v) m.extra = ((m.extra & 227) | (v << 2))
#define set_arspa(m,v) m.extra = ((m.extra & 31) | (v << 5))
#define set_arsha(m,v) set_eth_src(m,v)
#define get_sdn_haddr(m) get_eth_src(m)
#define get_sdn_nport(m) get_cid(m)
/*
#define get_sdn_haddr(m) (m.extra & 15)
#define get_sdn_nport(m) ((m.extra >> 4) & 15)
#define set_sdn_haddr(m,v) m.extra = ((m.extra & 240) | v)
#define set_sdn_nport(m,v) m.extra = ((m.extra & 15) | (v << 4))
*/
#define is_arp_message(m) (m.type == ARP_REQUEST || m.type == ARP_REPLY)

/* Utility IO macros */
#define host_send(m) channels[SDN_SWITCH_ID]!m
#define host_recv(m) channels[_pid]?m
#define switch_recv(m) channels[_pid]?m
#define switch_send(m) if \
			           :: port_to_mac[0]==get_eth_dst(m) -> channels[0]!m;\
			           :: port_to_mac[1]==get_eth_dst(m) -> channels[1]!m;\
			           :: port_to_mac[2]==get_eth_dst(m) -> channels[2]!m;\
			           fi 
#define switch_broadcast(m) channels[0]!m;\
						    channels[1]!m;\
						    channels[2]!m;
						    
/* 
 * Generic Utility.
 * Allow g_mac and g_mac_at to become a macro and g_ip to become local to the dhcp.
 */
#define g_mac(i) (i+1)
#define g_mac_at(i) (i+1)
#define get_my_mac() g_mac(_pid)
#define get_starting_ip() (_pid+1)

/*
 * Each machine has an arp table.
 * Each record pairs an IP Address with a MAC Address.
 */
typedef arp_table {
	byte data[COUNT_TOTAL];
};

/* The controller is not assigned identifiers, we suppose
 * that switch-controller communication is perfect
 */
/* Global data starting with "g_" is the ideal one*/
/* PROMELA Comment: unsigned : BITS arrays are interpreted as
 * unsigned char ones, thus they're written directly this way
 */
local byte g_ip[COUNT_TOTAL-1] = {1,2,3,4};
local byte g_mac_to_ip[COUNT_TOTAL] = {0,1,2,3,4};
local byte port_to_mac[COUNT_TOTAL-2] = {0,0,0};

/* ARP Tables and SDN flow rules */
arp_table arp_tbls[COUNT_TOTAL-1] = { ARP_NO_ENTRY };
#define get_arp(p,ip) arp_tbls[p].data[ip-1]
#define set_arp(p,ip,v) arp_tbls[p].data[ip-1] = v
#define get_my_arp(ip) get_arp(_pid,ip)
#define set_my_arp(ip,v) set_arp(_pid,ip,v)

/* 
 * All devices are connected to a SDN Switch
 * and the switch is connected to the Controllers
 */
chan channels[COUNT_TOTAL] = [MESSAGE_QUEUE_CAPACITY] of { message }; 

active proctype attacker() {
	local message m;
	
    do
    :: host_recv(m);
    ::
       atomic {
          if
          :: set_artha(m,2); 
          :: set_artha(m,3); 
          fi
          if
          :: set_arspa(m,2);
          :: set_arspa(m,3);
          fi
          if
          :: set_eth_src(m, get_my_mac());
          :: set_eth_src(m, 2);
          fi
       }
       ATOMIC {
          set_type(m, ARP_REPLY);
          set_eth_dst(m, ETH_BROADCAST);
          set_artha(m, 0);
          set_cid(m, _pid);
          host_send(m);
       }
    od
}

active proctype victim() {
	local message m;
	local byte myIP = get_starting_ip();
	do
	/* The host can always send an ARP request to anyone */
	/*::
	   if
	   :: set_artpa(m, 1);
	   :: set_artpa(m, 3);
	   :: set_artpa(m, 4);
	   fi
	   ATOMIC {
	   	   set_type(m, ARP_REQUEST);
		   set_eth_src(m, get_my_mac());
		   set_eth_dst(m, ETH_BROADCAST);
		   set_arspa(m, myIP);
		   set_artha(m, 0);
		   set_cid(m, _pid);
		   channels[SDN_SWITCH_ID]!m;
	   }*/
	:: host_recv(m) ->
	   if
	   /* VICTIM ARP START --------------------------------------------------*/
	   :: is_arp_message(m) ->
	      set_my_arp(get_arspa(m),get_arsha(m));
	      if
	      :: (get_artpa(m) == myIP) && (get_type(m) == ARP_REQUEST) ->
	         ATOMIC {
	            set_type(m, ARP_REPLY);
	            set_artha(m, get_arsha(m));
	            set_artpa(m, get_arspa(m));
	            set_arspa(m, myIP);
	            set_arsha(m, get_my_mac());
	            set_cid(m, _pid);
	            host_send(m);
	         }
	      :: else -> skip;
	      fi
	   /* VICTIM ARP END ----------------------------------------------------*/
	   fi
	od
}

active proctype dhcp_server() {
	local message m;
	local byte myIP = get_starting_ip();
	
	do
	:: host_recv(m) ->
	   if
	   /* DHCPSV ARP START --------------------------------------------------*/
	   :: is_arp_message(m) ->
	      set_my_arp(get_arspa(m),get_arsha(m));
	      if
	      :: (get_artpa(m) == myIP) && (get_type(m) == ARP_REQUEST) ->
	         ATOMIC {
	            set_type(m, ARP_REPLY);
	            set_artha(m, get_arsha(m));
	            set_artpa(m, get_arspa(m));
	            set_arspa(m, myIP);
	            set_arsha(m, get_my_mac());
	            set_cid(m, _pid);
	            host_send(m);
	         }
	      :: else -> skip; 
	      fi
	   /* DHCPSV ARP END ----------------------------------------------------*/
	   fi
	od
}

active proctype switch() {
	local message m;
	
sloop: do
    ::
    switch_recv(m)
       /* SWITCH FLOW CHANGE ------------------------------------------------*/
	   if
	   :: (get_type(m) != SDN_PACKET) && (port_to_mac[get_cid(m)] != get_eth_src(m)) ->
	      channels[SDN_CONTROLLER_ID]!m;
	      goto sloop;
	   :: (get_type(m) == SDN_PACKET) ->
	      d_step {
	      port_to_mac[get_sdn_nport(m)] = get_sdn_haddr(m);
		  set_type(m, get_originalType(m)); 
		  }
	   fi
       /* SWITCH FLOW CHANGE END---------------------------------------------*/
       
	   if
	   /* SWITCH ARP START --------------------------------------------------*/
	   :: is_arp_message(m) ->
	      set_my_arp(get_arspa(m),get_arsha(m));
	      if
	      :: (get_artpa(m) == SWITCH_IP) && (get_type(m) == ARP_REQUEST) ->
	         ATOMIC {
	            set_type(m, ARP_REPLY);
	            set_artha(m, get_arsha(m));
	            set_artpa(m, get_arspa(m));
	            set_arspa(m, SWITCH_IP);
	            set_arsha(m, get_my_mac());
	            set_eth_dst(m, get_artha(m));
	            set_cid(m, _pid);
	            switch_send(m);
	         }
	      :: (get_eth_dst(m) == ETH_BROADCAST) && (get_artpa(m) != SWITCH_IP) ->
	         ATOMIC { switch_broadcast(m) }
	      :: else -> skip; 
	      fi
	   /* SWITCH ARP END ----------------------------------------------------*/
	   fi
	od
}

active proctype controller() {
	local message m;
	/* 
	 * In this version, the sdn controller is somewhat basic. It just adds
	 * the flow rule following the packet_in event (we only consider the 
	 * hardware address here).
	 * In this abstraction this becomes equivalent to sending back the 
	 * packets that the switch sends to it.
	 * We do not delete it (implementing it inside the switch) because we
	 * want to consider all the possible delays for the flow rule to become
	 * active.
	 */
end:do
    :: host_recv(m) ->
       set_originalType(m, get_type(m));
       set_type(m, SDN_PACKET);
       host_send(m);
    od
}

ltl port_to_mac_binding { 
   []((port_to_mac[0] == g_mac_at(0) || port_to_mac[0] == 0) &&
	  (port_to_mac[1] == g_mac_at(1) || port_to_mac[1] == 0) &&
	  (port_to_mac[2] == g_mac_at(2) || port_to_mac[2] == 0) ) 
}

ltl arp_binding { 
   []((arp_tbls[0].data[3] == NO_ENTRY || g_mac_to_ip[arp_tbls[0].data[3]] == 4 || g_mac_to_ip[arp_tbls[0].data[3]] == NO_ENTRY) &&
	  (arp_tbls[1].data[3] == NO_ENTRY || g_mac_to_ip[arp_tbls[1].data[3]] == 4 || g_mac_to_ip[arp_tbls[1].data[3]] == NO_ENTRY) &&
	  (arp_tbls[2].data[3] == NO_ENTRY || g_mac_to_ip[arp_tbls[2].data[3]] == 4 || g_mac_to_ip[arp_tbls[2].data[3]] == NO_ENTRY) &&
	  (arp_tbls[3].data[3] == NO_ENTRY || g_mac_to_ip[arp_tbls[3].data[3]] == 4 || g_mac_to_ip[arp_tbls[3].data[3]] == NO_ENTRY) &&
	  (arp_tbls[0].data[0] == NO_ENTRY || g_mac_to_ip[arp_tbls[0].data[0]] == 1 || g_mac_to_ip[arp_tbls[0].data[0]] == NO_ENTRY) &&
	  (arp_tbls[1].data[0] == NO_ENTRY || g_mac_to_ip[arp_tbls[1].data[0]] == 1 || g_mac_to_ip[arp_tbls[1].data[0]] == NO_ENTRY) &&
	  (arp_tbls[2].data[0] == NO_ENTRY || g_mac_to_ip[arp_tbls[2].data[0]] == 1 || g_mac_to_ip[arp_tbls[2].data[0]] == NO_ENTRY) &&
	  (arp_tbls[3].data[0] == NO_ENTRY || g_mac_to_ip[arp_tbls[3].data[0]] == 1 || g_mac_to_ip[arp_tbls[3].data[0]] == NO_ENTRY) &&
	  (arp_tbls[0].data[1] == NO_ENTRY || g_mac_to_ip[arp_tbls[0].data[1]] == 2 || g_mac_to_ip[arp_tbls[0].data[1]] == NO_ENTRY) &&
	  (arp_tbls[1].data[1] == NO_ENTRY || g_mac_to_ip[arp_tbls[1].data[1]] == 2 || g_mac_to_ip[arp_tbls[1].data[1]] == NO_ENTRY) &&
	  (arp_tbls[2].data[1] == NO_ENTRY || g_mac_to_ip[arp_tbls[2].data[1]] == 2 || g_mac_to_ip[arp_tbls[2].data[1]] == NO_ENTRY) &&
	  (arp_tbls[3].data[1] == NO_ENTRY || g_mac_to_ip[arp_tbls[3].data[1]] == 2 || g_mac_to_ip[arp_tbls[3].data[1]] == NO_ENTRY) &&
	  (arp_tbls[0].data[2] == NO_ENTRY || g_mac_to_ip[arp_tbls[0].data[2]] == 3 || g_mac_to_ip[arp_tbls[0].data[2]] == NO_ENTRY) &&
	  (arp_tbls[1].data[2] == NO_ENTRY || g_mac_to_ip[arp_tbls[1].data[2]] == 3 || g_mac_to_ip[arp_tbls[1].data[2]] == NO_ENTRY) &&
	  (arp_tbls[2].data[2] == NO_ENTRY || g_mac_to_ip[arp_tbls[2].data[2]] == 3 || g_mac_to_ip[arp_tbls[2].data[2]] == NO_ENTRY) &&
	  (arp_tbls[3].data[2] == NO_ENTRY || g_mac_to_ip[arp_tbls[3].data[2]] == 3 || g_mac_to_ip[arp_tbls[3].data[2]] == NO_ENTRY) )
}
