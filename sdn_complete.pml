/* Count end-host MUST be at least 2. */
#define COUNT_ENDHOST 3
#define COUNT_TOTAL COUNT_ENDHOST + 2
#define ATTACKER_ID 0
#define SDN_SWITCH_ID COUNT_TOTAL - 2
#define SDN_CONTROLLER_ID COUNT_TOTAL - 1
#define DHCP_SERVER_ID COUNT_TOTAL - 3
#define ARP_NO_ENTRY 0
#define NO_ENTRY 0
#define IP_BROADCAST 255
#define ETH_BROADCAST 255
#define IP_NULL      0
#define MESSAGE_QUEUE_CAPACITY 2
#define IP_BITS 3
#define MAC_BITS 3
#define PORT_BITS 3
#define ACTOR_BITS 3

mtype:arp_type = { ARP_REQUEST, ARP_REPLY }
/* We do not include DHCP_INFORM packets */
mtype:dhcp_type = { DHCP_OFFER, DHCP_DISCOVER, DHCP_REQUEST, DHCP_ACK, 
                    DHCP_NAK, DHCP_DECLINE, DHCP_RELEASE }

/*
 * We only consider IP packets (which will be DHCP packets),
 * ICMP packets and Address Resolution frames.
 */
mtype:msg_type = { IP_PACKET, ARP_PACKET, ICMP_PING, SDN_PACKET }

typedef eth_header {
	byte eth_src;
	byte eth_dst;
};

typedef ip_header {
	byte nw_src;
	byte nw_dst;
};

typedef dhcp_fields {
	mtype:dhcp_type type
	/* 
	 * We're excluding several fields, also usually important ones like lease time.
	 * We also skip giaddr, ciaddr, boot fields. Also "xid" is skipped.
	 * In our attack model, the attacker cannot control the switches, thus we 
	 * can assume that the nonce cannot be retrieved by it (and so we identify
	 * checking the packed cid directly)
	 */
	/*
	  client ip address (only BOUND, RENEW, REBINDING
	  ... and that's the reason we're commenting it now) 
	unsigned ciaddr; 
	*/
	byte chaddr; /* client hardware address */
	byte yiaddr; /* YOUR ip address */
	byte siaddr; /* SERVER ip address */
};

typedef arp_fields {
	/*
	 * The following fields are omitted to save space.
	 * They are not useful anyway.
	 * - arhrd (Hardware address space)
	 * - arpro (Protocol address space)
	 * - arhln (Hardware address unsigned-length)
	 * - arpln (Protocol address unsigned-length)
	 */
	mtype:arp_type arop; /* AR operation */
	byte arsha; /* sender hardware address */
	byte arspa; /* sender protocol address */
	byte artha; /* target hardware address */
	byte artpa; /* target protocol address */
};

typedef sdn_fields {
	byte haddr;
	byte nport;
	mtype:msg_type originalType;
};

/*
 * We put everything on a single structure in order
 * to have a single channel for each host.
 */
typedef msg {
	eth_header frame;
	mtype:msg_type type;
	arp_fields arp;
	dhcp_fields dhcp;
	ip_header ip;
	byte cid;
	sdn_fields sdn;
};

/*
 * Each machine has an arp table.
 * Each record pairs an IP Address with a MAC Address.
 */
typedef arp_table {
	byte data[COUNT_TOTAL];
};

/* The controller is not assigned identifiers, we suppose
 * that switch-controller communication is perfect
 */
/* Global data starting with "g_" is the ideal one*/
/* PROMELA Comment: unsigned : BITS arrays are interpreted as
 * unsigned char ones, thus they're written directly this way
 */
byte g_mac[COUNT_TOTAL-1] = {1,2,3,4};
local byte g_mac_at[COUNT_TOTAL-2] = {1,2,3};
byte g_ip[COUNT_TOTAL-1] = {1,2,3,4};
local byte g_mac_to_ip[COUNT_TOTAL] = {0,1,2,3,4};

/* ARP Tables and SDN flow rules */
arp_table arp_tbls[COUNT_TOTAL-1] = { ARP_NO_ENTRY };
/* 
 * We assume that the flow table just maps MAC addresses
 * to their network port in the switch
 */
local byte port_to_mac[COUNT_TOTAL-2] = {0,0,0};

/* 
 * All devices are connected to a SDN Switch
 * and the switch is connected to the Controllers
 */
chan channels[COUNT_TOTAL] = [MESSAGE_QUEUE_CAPACITY] of { msg }; 

active proctype attacker() {
	local unsigned procid : ACTOR_BITS = _pid;
	local msg m;
end:do
	:: nempty(channels[procid]) ->
	   channels[procid] ? m;
	   if 
	   :: m.type == IP_PACKET && m.dhcp.type == DHCP_DISCOVER ->
	      atomic {
	           m.frame.eth_src = g_mac[procid];
	           m.frame.eth_dst = g_mac[0];
	           m.type = IP_PACKET;
	           m.dhcp.type = DHCP_OFFER;
	           m.dhcp.chaddr = g_mac[0];
	           m.dhcp.yiaddr = 2;
	           m.dhcp.siaddr = g_ip[_pid];
	       	   channels[SDN_SWITCH_ID]!m;
           }
	   :: skip 
	   fi
	:: 
	   if
	   :: m.dhcp.chaddr = 2
	   :: m.dhcp.chaddr = 3
	   fi
	   d_step {
	      m.type = IP_PACKET;
	      m.ip.nw_src = 1;
	      m.ip.nw_dst = g_ip[DHCP_SERVER_ID];
	      m.dhcp.type = DHCP_RELEASE;
	      m.frame.eth_src = g_mac[procid];
	      m.frame.eth_dst = g_mac[SDN_SWITCH_ID];
	      m.cid = procid;
	   }
	   channels[SDN_SWITCH_ID]!m;
	:: 
	   atomic {
	   	   if
		   :: m.arp.artpa = 2;
		   :: m.arp.artpa = 3;
		   :: m.arp.artpa = 4;
		   fi
	   }
	   d_step {
		   m.type = ARP_PACKET;
		   m.frame.eth_src = g_mac[procid];
		   m.frame.eth_dst = ETH_BROADCAST;
		   m.arp.arop = ARP_REQUEST;
		   m.arp.arspa = m.arp.artpa;
		   m.arp.arsha = g_mac[procid];
		   m.arp.artha = 0;
		   m.cid = procid;
	    }
		channels[SDN_SWITCH_ID]!m;
	::
	   atomic {
	   if
	   :: m.arp.artha = 2
	   :: m.arp.artha = 3
	   fi
	   if
	   :: m.arp.arspa = 2
	   :: m.arp.arspa = 3
	   fi
	   if
	   :: m.arp.arsha = 2
	   :: m.arp.arsha = 3
	   fi
	   d_step {
	      m.arp.arop = ARP_REPLY;
	      m.arp.artha = m.arp.arsha;
	      m.arp.artpa = m.arp.arspa;
	      m.frame.eth_src = m.arp.arsha;
	      m.frame.eth_dst = m.arp.artha;
	      m.cid = procid;   
	   }
	   channels[SDN_SWITCH_ID]!m;
	   }
	od
}

active[COUNT_ENDHOST-2] proctype end_host() {
	local unsigned procid : ACTOR_BITS = _pid;
	local msg m;
	local unsigned myIP : IP_BITS = g_ip[procid];
	/*
	 * dhcpcdStates
	 * 0 - Starting State
	 * 1 - Request Sent
	 */
	local bit dhcpcdState = 0;
end:do
	/* The host can always send an ARP request to anyone */
	:: 
	   atomic {
		   m.type = ARP_PACKET;
		   m.frame.eth_src = g_mac[procid];
		   m.frame.eth_dst = ETH_BROADCAST;
		   m.arp.arop = ARP_REQUEST;
		   m.arp.arspa = myIP;
		   m.arp.arsha = g_mac[procid];
		   m.arp.artha = 0;
		   m.cid = procid;
		   if
		   :: myIP != 1 -> m.arp.artpa = 1;
		   :: myIP != 2 -> m.arp.artpa = 2;
		   :: myIP != 3 -> m.arp.artpa = 3;
		   :: myIP != 4 -> m.arp.artpa = 4;
		   fi
		   channels[SDN_SWITCH_ID]!m;
	   } 
	/* 
	 * If the machine has not an IP address, it asks for one.
	 * If it has one, it might ask for release.
	 */
    :: myIP == 0 ->
       atomic {
	       m.type = IP_PACKET;
	       m.ip.nw_src = 0;
	       m.ip.nw_dst = IP_BROADCAST;
	       m.dhcp.type = DHCP_DISCOVER;
	       m.dhcp.chaddr = g_mac[procid];
	       m.frame.eth_src = g_mac[procid];
	       m.frame.eth_dst = g_mac[SDN_SWITCH_ID];
	       m.cid = procid;
	       channels[SDN_SWITCH_ID]!m;
	       dhcpcdState=2;
       }
    :: myIP != 0 -> 
       atomic {
	       m.type = IP_PACKET;
	       m.ip.nw_src = myIP;
	       m.ip.nw_dst = IP_BROADCAST;
	       m.dhcp.type = DHCP_RELEASE;
	       m.dhcp.chaddr = g_mac[procid];
	       m.frame.eth_src = g_mac[procid];
	       m.frame.eth_dst = g_mac[SDN_SWITCH_ID];
	       m.cid = procid;
	       channels[SDN_SWITCH_ID]!m;
	       myIP = 0;
       }
	:: nempty(channels[procid]) -> 
	   channels[procid] ? m;
	   atomic {
	   if
	   /* This models Address Resolution responses */
	   :: m.type == ARP_PACKET ->
	      atomic {
		  /* I'm following RFC826 specification here assuming the  */
		  /* hardware and the protocol are supported in the client */
		  /* We do not use the merge flag here since we have a full*/
		  /* table.                                                */
		  /* The first step is to add(/merge) the record.          */
		  /*if
		  :: arp_tbls[procid].data[m.arp.arspa] != ARP_NO_ENTRY ->*/
		     arp_tbls[procid].data[m.arp.arspa] = m.arp.arsha;
		  /*:: else -> skip
		  fi */
		  /* If the host has the target protocol address, it updates   */
		  /* the table and then if the packet is a request, it replies */
		  if 
		  :: m.arp.artpa == myIP ->
		     /*arp_tbls[procid].data[m.arp.arspa] = m.arp.arsha;*/
		     /* Only NOW we check if it is a REQUEST*/
			 if
			 :: m.arp.arop == ARP_REQUEST -> 
			    /* The response should be deterministic and atomic (performance reasons)*/
			    atomic {
				    m.arp.arop = ARP_REPLY;
				    m.arp.artha = m.arp.arsha;
				    m.arp.artpa = m.arp.arspa;
				    m.arp.arspa = myIP;
				    m.arp.arsha = g_mac[procid];
				    m.frame.eth_src = g_mac[procid];
				    m.frame.eth_dst = m.arp.artha;
				    m.cid = procid;
				    channels[SDN_SWITCH_ID]!m;
			    }
			 :: else -> skip;
			 fi
		  :: else -> skip;
		  fi
		  }
	   /* We have dhcpcd here (a cut down version at least) */
	   :: m.type == IP_PACKET ->
	      if
	      :: m.dhcp.type == DHCP_OFFER && dhcpcdState == 2 ->
	         /* Blindly accept any offer sending a request */
	         d_step {
		         dhcpcdState = 1;
		         m.dhcp.type = DHCP_REQUEST;
		         m.dhcp.chaddr = g_mac[procid];
		         m.dhcp.siaddr = m.ip.nw_src;
		         m.ip.nw_src = 0;
		         m.ip.nw_dst = IP_BROADCAST;
		         m.frame.eth_src = g_mac[procid];
	             m.frame.eth_dst = g_mac[SDN_SWITCH_ID];
	             m.cid = procid;
	         }
	         channels[SDN_SWITCH_ID]!m;
	      :: m.dhcp.type == DHCP_ACK && dhcpcdState == 1 ->
	         d_step {
	            dhcpcdState = 0;
	            myIP = m.dhcp.yiaddr;
	         }
	      /* 
	       * DHCP_NAK should restart the request phase, 
	       * we just ignore it instead, it shall not happen
	       */
	      :: else -> skip;
	      fi 
	   /*:: m.type == ICMP_PING ->
		  atomic {
		      m.ip.nw_dst = m.ip.nw_src;
		      m.ip.nw_src = myIP;
		      m.frame.eth_dst = m.frame.eth_src;
		      m.frame.eth_src = g_mac[procid];
		      m.cid = procid;
		      channels[SDN_SWITCH_ID]!m;
	      }*/
	   fi 
	   }
	od
}

active proctype dhcp_server() {
	local unsigned procid : ACTOR_BITS = _pid;
	local unsigned myIP : IP_BITS = g_ip[procid];
	local msg m;
	/* Boolean values that track assigned addresses */
	local bool assignedAddresses[5] = { true, true, true, true, true };
	/* Tracks addresses proposed to hosts */
	local byte proposedAddresses[COUNT_ENDHOST-1] = { 0 };
    /* Verified proposed addresses */
    local bool shouldVerify[COUNT_ENDHOST-1] = { false, false }
end:do
    /* 
     * These two cases model the timeout of the ARP request 
     * sent to check if an address is already in use.
     * In these cases, a DHCP_OFFER is sent.
     */
    :: shouldVerify[0] ->
       atomic {
           shouldVerify[0] = false;
           m.frame.eth_src = g_mac[procid];
           m.frame.eth_dst = g_mac[0];
           m.type = IP_PACKET;
           m.dhcp.type = DHCP_OFFER;
           m.dhcp.chaddr = g_mac[0];
           m.dhcp.yiaddr = proposedAddresses[0];
           assignedAddresses[proposedAddresses[0]] = false;
           m.dhcp.siaddr = myIP;
       channels[SDN_SWITCH_ID]!m;
       }
       skip;
    :: shouldVerify[1] ->
       atomic {
           shouldVerify[1] = false;
           m.frame.eth_src = g_mac[procid];
           m.frame.eth_dst = g_mac[1];
           m.type = IP_PACKET;
           m.dhcp.type = DHCP_OFFER;
           m.dhcp.chaddr = g_mac[1];
           m.dhcp.yiaddr = proposedAddresses[1];
           assignedAddresses[proposedAddresses[1]] = false;
           m.dhcp.siaddr = myIP;
       channels[SDN_SWITCH_ID]!m;
       }
       skip;
    :: nempty(channels[procid]) ->
	   channels[procid] ? m;
	   atomic {
	   if
	   /* This models Address Resolution responses */
	   :: m.type == ARP_PACKET ->
	      atomic {
		  /* I'm following RFC826 specification here assuming the  */
		  /* hardware and the protocol are supported in the client */
		  /* We do not use the merge flag here since we have a full*/
		  /* table.                                                */
		  /* The first step is to add(/merge) the record.          */
		  /*if
		  :: arp_tbls[procid].data[m.arp.arspa] != ARP_NO_ENTRY ->*/
		     arp_tbls[procid].data[m.arp.arspa] = m.arp.arsha;
		  /*:: else -> skip;
		  fi*/
		  atomic {
		  	if
		  	:: proposedAddresses[0] == m.arp.arsha ->
		  	   shouldVerify[0] = false;
		  	   proposedAddresses[0] = 0;
		  	   assignedAddresses[m.arp.arsha] = true;
		  	:: proposedAddresses[1] == m.arp.arsha ->
		  	   shouldVerify[1] = false;
		  	   proposedAddresses[1] = 0;
		  	   assignedAddresses[m.arp.arsha] = true;
		  	:: else -> skip;
		  	fi
		  }
		  if
		  :: else -> skip;
		  fi
		  /* If the host has the target protocol address, it updates   */
		  /* the table and then if the packet is a request, it replies */
		  if 
		  :: m.arp.artpa == myIP ->
		     /*arp_tbls[procid].data[m.arp.arspa] = m.arp.arsha;*/
		     /* Only NOW we check if it is a REQUEST*/
			 if
			 :: m.arp.arop == ARP_REQUEST -> 
			    /* The response should be deterministic and atomic (performance reasons)*/
			    atomic {
				    m.arp.arop = ARP_REPLY;
				    m.arp.artha = m.arp.arsha;
				    m.arp.artpa = m.arp.arspa;
				    m.arp.arspa = myIP;
				    m.arp.arsha = g_mac[procid];
				    m.frame.eth_src = g_mac[procid];
				    m.frame.eth_dst = m.arp.artha;
				    m.cid = procid;
				    channels[SDN_SWITCH_ID]!m;
			    }
			 :: else -> skip;
			 fi
		  :: else -> skip;
		  fi
		  }
	   :: m.type == IP_PACKET ->
	   /*
	    * Here we implement a cut down version of a dhcp daemon.
	    * We do not follow some of the SHOULDs of RFC 2131 like
	    * the pool of previously assigned addresses (through dhcp
	    * discover starving, offers for any free address can be 
	    * obtained)
	    * We also remove the nonce 'xid' and identify sessions solely
	    * by the chaddr.                    This is enough since
	    * the attacks that are based on sending multiple DHCP_DISCOVER
	    * (starting multiple sessions) are futile in this context.
	    */
	   atomic {
	   if
	   :: m.dhcp.type == DHCP_DISCOVER ->
	      /*
	       * We select a free address (if there's one).
	       * When multiple patterns in the guards match, d_step
	       * has a deterministic behavior, and that's acceptable
	       * (if not desirable) here.
	       */
	      atomic {
	         if
	         :: !assignedAddresses[1] -> proposedAddresses[m.dhcp.chaddr-1] = 1;
	         :: !assignedAddresses[2] -> proposedAddresses[m.dhcp.chaddr-1] = 2;
	         :: !assignedAddresses[3] -> proposedAddresses[m.dhcp.chaddr-1] = 3;
	         :: !assignedAddresses[4] -> proposedAddresses[m.dhcp.chaddr-1] = 4;
	         :: else -> proposedAddresses[m.dhcp.chaddr-1] = 0;
	         fi
	      }
	      if
	      /* 
	       * When allocating a new address, servers SHOULD check that 
	       * the offered network address is not already in use.
	       * We do this by sending an ARP request. If the request is 
	       * answered we abort, else we send a DHCP_OFFER.
      	   */
	      :: proposedAddresses[m.dhcp.chaddr-1] != 0 ->
	         atomic {
	         	shouldVerify[m.dhcp.chaddr-1] = true;
	         	m.type = ARP_PACKET;
			    m.frame.eth_src = g_mac[procid];
			    m.frame.eth_dst = ETH_BROADCAST;
			    m.arp.arop = ARP_REQUEST;
			    m.arp.arspa = myIP;
			    m.arp.arsha = g_mac[procid];
			    m.arp.artha = 0;
			    m.arp.artpa = proposedAddresses[m.dhcp.chaddr-1];
			    m.cid = procid;
	         channels[SDN_SWITCH_ID]!m;
	         }
	      :: else -> skip;
	      fi
	   :: m.dhcp.type == DHCP_REQUEST ->
	      if
	      :: m.dhcp.yiaddr == proposedAddresses[m.cid] -> 
	         /* send dhcp ack and update global table */
	         atomic {
	             g_mac_to_ip[g_mac[m.cid]] = m.dhcp.yiaddr;
	             m.cid = procid;
	             m.dhcp.siaddr = myIP;
	             m.ip.nw_dst = IP_BROADCAST;
	             m.ip.nw_src = myIP;
	             m.dhcp.type = DHCP_ACK;
	             g_ip[m.dhcp.chaddr-1] = m.dhcp.yiaddr;
	             proposedAddresses[m.dhcp.chaddr-1] = 0;
	             assignedAddresses[m.dhcp.yiaddr] = true;
	         channels[SDN_SWITCH_ID]!m;
	         }
	      :: else ->
	         atomic {
	     		 m.cid = procid;
	             m.dhcp.siaddr = myIP;
	             m.ip.nw_dst = IP_BROADCAST;
	             m.ip.nw_src = myIP;
	        	 m.dhcp.type = DHCP_NAK;
	         channels[SDN_SWITCH_ID]!m;
	         }
	      fi
	   :: m.dhcp.type == DHCP_RELEASE ->
	   	  /*assert(m.dhcp.chaddr == g_mac[m.cid]);*/
	   	  g_ip[m.dhcp.chaddr-1] = 0;
	      /* Theoretically it should send a DHCP_ACK back,
	         we ignore this step since end_hosts won't use it. */
	   :: else -> skip;
	   fi
	   }
	   /*:: m.type == ICMP_PING ->
		  atomic {
		      m.ip.nw_dst = m.ip.nw_src;
		      m.ip.nw_src = myIP;
		      m.frame.eth_dst = m.frame.eth_src;
		      m.frame.eth_src = g_mac[procid];
		      m.cid = procid;
		      channels[SDN_SWITCH_ID]!m;
	      }*/
	   fi
	   }
	od
}

#define sdn_switch_send(m) atomic { \
                             if \
					         :: port_to_mac[0]==m.frame.eth_dst -> channels[0]!m;\
					         :: port_to_mac[1]==m.frame.eth_dst -> channels[1]!m;\
					         :: port_to_mac[2]==m.frame.eth_dst -> channels[2]!m;\
					         fi \
					       }
					      
active proctype sdn_switch() {
	local unsigned procid : ACTOR_BITS = _pid;
	local msg m;
	local unsigned myIP : IP_BITS = g_ip[SDN_SWITCH_ID];
end:do
    :: true -> 
sloop: channels[procid] ? m;
	   /* 
	    * Tell the controller information about the received packet 
	    * if it generates a flow table miss. 
	    * This happens when the network location (which equals the message 
	    * cid here) is not bound to the hardware address of the message.
	    */
	   if
	   :: (m.type != SDN_PACKET) && (port_to_mac[m.cid] != m.frame.eth_src) ->
	      /* 
	       * Send a notification to the controller. 
	       * NOTE: The switch does not update the flow rules. It waits
	       * for the controller's instructions.
	       * We skip many of the fields of the packet, we assume that only
	       * the switch can genuinely communicate with the controller.
	       */
	      atomic {
	          /*m2.type = SDN_PACKET;
	          m2.sdn.haddr = m.frame.eth_src;
	          m2.sdn.nport = m.cid;
	          channels[SDN_CONTROLLER_ID]!m2;*/
	          m.sdn.haddr = m.frame.eth_src;
	          m.sdn.nport = m.cid;
	          channels[SDN_CONTROLLER_ID]!m;
	      }
	      goto sloop;
	   :: else -> skip;
	   fi
	   atomic {
	   /* This models flow change packets (with packet out embedded)*/
	   if
	   :: m.type == SDN_PACKET ->
	      d_step {
		      port_to_mac[m.sdn.nport] = m.sdn.haddr;
		      m.type = m.sdn.originalType;
	      }
	   :: else -> skip;
	   fi
	   /* Finally handle the message*/
	   if
	   /* This models Address Resolution responses */
	   :: m.type == ARP_PACKET ->
	      atomic {
		  /* I'm following RFC826 specification here assuming the   */
		  /* hardware and the protocol are supported in the client  */
		  /* We do not use the merge flag here since we have a full */
		  /* table.                                                 */
		  /* The first step is to add(/merge) the record.           */
		  /*if
		  :: arp_tbls[procid].data[m.arp.arspa] != ARP_NO_ENTRY ->*/
		     arp_tbls[procid].data[m.arp.arspa] = m.arp.arsha;
		  /*:: else -> skip;
		  fi */
		  /* If the host has the target protocol address, it updates   */
		  /* the table and then if the packet is a request, it replies */
		  if 
		  :: m.arp.artpa == myIP ->
		     /*arp_tbls[procid].data[m.arp.arspa] = m.arp.arsha;*/
		     /* Only NOW we check if it is a REQUEST*/
			 if
			 :: m.arp.arop == ARP_REQUEST -> 
			    /* The response should be deterministic and atomic*/
			    d_step {
				    m.arp.arop = ARP_REPLY;
				    m.arp.artha = m.arp.arsha;
				    m.arp.artpa = m.arp.arspa;
				    m.arp.arspa = myIP;
				    m.arp.arsha = g_mac[procid];
				    m.frame.eth_src = g_mac[procid];
				    m.frame.eth_dst = m.arp.artha;
				    m.cid = procid;
			    }
				sdn_switch_send(m);
			 fi
		  :: else -> skip;
		  fi
		  if
	      :: m.frame.eth_dst == ETH_BROADCAST ->
	         atomic {
				 channels[0]!m;
				 channels[1]!m;
				 channels[2]!m;
			 }
	      :: else -> skip;
		  fi
		  }
	   :: m.type == IP_PACKET -> 
	      assert((m.cid != DHCP_SERVER_ID && (m.dhcp.type==DHCP_DISCOVER|| m.dhcp.type==DHCP_REQUEST || m.dhcp.type==DHCP_DECLINE || m.dhcp.type ==DHCP_RELEASE)))
	      if 
	      :: (m.ip.nw_dst != myIP) && (m.ip.nw_dst != IP_BROADCAST) ->
	         d_step {
		         m.frame.eth_src = g_mac[procid];
		         m.frame.eth_dst = arp_tbls[procid].data[m.ip.nw_dst];
	         }
		     sdn_switch_send(m);
	      :: (m.ip.nw_dst == IP_BROADCAST) ->
	         atomic {
	             m.frame.eth_src = g_mac[procid];
		         atomic {
					 channels[0]!m;
					 channels[1]!m;
					 channels[2]!m;
				 }
	         }
	      fi
	   /*:: m.type == ICMP_PING ->
		  d_step {
		      m.ip.nw_dst = m.ip.nw_src;
		      m.ip.nw_src = myIP;
		      m.frame.eth_dst = m.frame.eth_src;
		      m.frame.eth_src = g_mac[procid];
		      m.cid = procid;
	      }
		  sdn_switch_send(m);*/
	   fi
	   }
	od
}

active proctype sdn_controller() {
	unsigned procid : ACTOR_BITS = _pid;
	msg m;
	/* 
	 * In this version, the sdn controller is somewhat basic. It just adds
	 * the flow rule following the packet_in event (we only consider the 
	 * hardware address here).
	 * In this abstraction this becomes equivalent to sending back the 
	 * packets that the switch sends to it.
	 * We do not delete it (implementing it inside the switch) because we
	 * want to consider all the possible delays for the flow rule to become
	 * active.
	 */
end:do
	:: nempty(channels[procid]) ->
	   channels[procid] ? m;
	   atomic {
		   m.sdn.originalType = m.type; 
		   m.type = SDN_PACKET;
		   channels[SDN_SWITCH_ID] ! m;
	   }
	od
}

ltl port_to_mac_binding { 
   []((port_to_mac[0] == g_mac_at[0] || port_to_mac[0] == 0) &&
	  (port_to_mac[1] == g_mac_at[1] || port_to_mac[1] == 0) &&
	  (port_to_mac[2] == g_mac_at[2] || port_to_mac[2] == 0) ) 
}

ltl arp_binding { 
   []((arp_tbls[0].data[3] == NO_ENTRY || g_mac_to_ip[arp_tbls[0].data[3]] == 3 || g_mac_to_ip[arp_tbls[0].data[3]] == NO_ENTRY) &&
	  (arp_tbls[1].data[3] == NO_ENTRY || g_mac_to_ip[arp_tbls[1].data[3]] == 3 || g_mac_to_ip[arp_tbls[1].data[3]] == NO_ENTRY) &&
	  (arp_tbls[2].data[3] == NO_ENTRY || g_mac_to_ip[arp_tbls[2].data[3]] == 3 || g_mac_to_ip[arp_tbls[2].data[3]] == NO_ENTRY) &&
	  (arp_tbls[3].data[3] == NO_ENTRY || g_mac_to_ip[arp_tbls[3].data[3]] == 3 || g_mac_to_ip[arp_tbls[3].data[3]] == NO_ENTRY) &&
	  (arp_tbls[0].data[0] == NO_ENTRY || g_mac_to_ip[arp_tbls[0].data[0]] == 0 || g_mac_to_ip[arp_tbls[0].data[0]] == NO_ENTRY) &&
	  (arp_tbls[1].data[0] == NO_ENTRY || g_mac_to_ip[arp_tbls[1].data[0]] == 0 || g_mac_to_ip[arp_tbls[1].data[0]] == NO_ENTRY) &&
	  (arp_tbls[2].data[0] == NO_ENTRY || g_mac_to_ip[arp_tbls[2].data[0]] == 0 || g_mac_to_ip[arp_tbls[2].data[0]] == NO_ENTRY) &&
	  (arp_tbls[3].data[0] == NO_ENTRY || g_mac_to_ip[arp_tbls[3].data[0]] == 0 || g_mac_to_ip[arp_tbls[3].data[0]] == NO_ENTRY) &&
	  (arp_tbls[0].data[1] == NO_ENTRY || g_mac_to_ip[arp_tbls[0].data[1]] == 1 || g_mac_to_ip[arp_tbls[0].data[1]] == NO_ENTRY) &&
	  (arp_tbls[1].data[1] == NO_ENTRY || g_mac_to_ip[arp_tbls[1].data[1]] == 1 || g_mac_to_ip[arp_tbls[1].data[1]] == NO_ENTRY) &&
	  (arp_tbls[2].data[1] == NO_ENTRY || g_mac_to_ip[arp_tbls[2].data[1]] == 1 || g_mac_to_ip[arp_tbls[2].data[1]] == NO_ENTRY) &&
	  (arp_tbls[3].data[1] == NO_ENTRY || g_mac_to_ip[arp_tbls[3].data[1]] == 1 || g_mac_to_ip[arp_tbls[3].data[1]] == NO_ENTRY) &&
	  (arp_tbls[0].data[2] == NO_ENTRY || g_mac_to_ip[arp_tbls[0].data[2]] == 2 || g_mac_to_ip[arp_tbls[0].data[2]] == NO_ENTRY) &&
	  (arp_tbls[1].data[2] == NO_ENTRY || g_mac_to_ip[arp_tbls[1].data[2]] == 2 || g_mac_to_ip[arp_tbls[1].data[2]] == NO_ENTRY) &&
	  (arp_tbls[2].data[2] == NO_ENTRY || g_mac_to_ip[arp_tbls[2].data[2]] == 2 || g_mac_to_ip[arp_tbls[2].data[2]] == NO_ENTRY) &&
	  (arp_tbls[3].data[2] == NO_ENTRY || g_mac_to_ip[arp_tbls[3].data[2]] == 2 || g_mac_to_ip[arp_tbls[3].data[2]] == NO_ENTRY) )/*&&
	  (arp_tbls[0].data[4] == NO_ENTRY || g_mac_to_ip[arp_tbls[0].data[4]] == 4 || g_mac_to_ip[arp_tbls[0].data[4]] == NO_ENTRY) &&
	  (arp_tbls[1].data[4] == NO_ENTRY || g_mac_to_ip[arp_tbls[1].data[4]] == 4 || g_mac_to_ip[arp_tbls[1].data[4]] == NO_ENTRY) &&
	  (arp_tbls[2].data[4] == NO_ENTRY || g_mac_to_ip[arp_tbls[2].data[4]] == 4 || g_mac_to_ip[arp_tbls[2].data[4]] == NO_ENTRY) &&
	  (arp_tbls[3].data[4] == NO_ENTRY || g_mac_to_ip[arp_tbls[3].data[4]] == 4 || g_mac_to_ip[arp_tbls[3].data[4]] == NO_ENTRY) )*/
}
